using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// DataLoader searches the StreamingAssets folder for the required files to load into database.
/// TODO: interface with ModLoader/ModManager.
/// </summary>
public class DataLoader<T> where T : IModdable, new() {
    FileInfo targetFile;

    /// <summary>
    /// Non-default constructor
    /// </summary>
    /// <param name="targetFile"></param> Pass the desired file to read data from
    public DataLoader(FileInfo targetFile) {
        this.targetFile = targetFile;
    }

    /// <summary>
    /// Returns a dictionary of objects loaded from JSON file. If no object returns empty dictionary.
    /// </summary>
    /// <returns>Dictionary of objects</returns>
    public void LoadDataInto(DataMap<T> dataMap) {
        var text = File.ReadAllText(targetFile.FullName);

        var vals = JsonConvert.DeserializeObject<List<T>>(text);

        foreach (var item in vals) {
            //Debug.Log("Name: " + item.Name);
            dataMap.Add(item);
        }
    }

    /// <summary>
    /// Reads the Target File of the DataLoader and extracts objects of the designated type to add to Data Map.
    /// </summary>
    /// <param name="dataMap"></param>
    /// <param name="type"></param>
    public void LoadLinqDataInto(DataMap<T> dataMap, string type) {
        StreamReader streamRead = new StreamReader(targetFile.FullName);
        JsonTextReader reader = new JsonTextReader(streamRead);
        reader.SupportMultipleContent = true;
        //Allows for reading multiple objects in the format {Object 1}{Object 2}...{Object n}  
        //Creating Json in this format allows modders to add objects easily as individual sections such as {Floor}{Object}{Floor} without worrying about Json array formatting

        while (true) {
            if (!reader.Read()) {
                break;
            }

            JsonSerializer serializer = new JsonSerializer();

            JObject jObj = serializer.Deserialize<JObject>(reader);

            if (jObj[type] != null) {
                JToken data = jObj[type];

                var newData = new T();

                newData.ReadDataJSON(data);
                newData.Type = type;

                dataMap.Add(newData);
            }
        }
    }

}
