using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// DataManager holds values for all the data loaded in from json files (objects, needs, jobs etc).
/// </summary>
public class DataManager : MonoBehaviour {
    public static DataManager instance { get; private set; }

    public DataMap<Floor> Floor { get; private set; }
    public DataMap<Wall> Wall { get; private set; }
    public DataMap<Item> BuyObjects { get; private set; }

    [Header("Data Files")]
    [SerializeField] string[] folderData;
    [SerializeField] string materialsData;

    /// <summary>
    /// Awake runs once on game load to initialise.
    /// </summary>
    public void Awake() {
        if (instance == null) {
            instance = this;
        }
        else {
            Debug.LogWarning("Multiple copies of DataManager exist");
            Destroy(this);
        }

        Initialise();

        LoadMaterials();

        Debug.Log("Data Load Complete"); 
    }

    /// <summary>
    /// Loads the data from files into individual Data Maps
    /// </summary>
    private void LoadMaterials() {
        Debug.Log("LoadMaterials()");
        new DataLoader<Floor>(GetFile(folderData, materialsData)).LoadLinqDataInto(Floor, "Floor");
        new DataLoader<Wall>(GetFile(folderData, materialsData)).LoadLinqDataInto(Wall, "Wall");
        new DataLoader<Item>(GetFile(folderData, materialsData)).LoadLinqDataInto(BuyObjects, "Object");
    }

    /// <summary>
    /// Initialises the DataMaps
    /// </summary>
    private void Initialise() {
        Floor = new DataMap<Floor>();
        Wall = new DataMap<Wall>();
        BuyObjects = new DataMap<Item>();
    }

    /// <summary>
    /// Combines filename and folder to get the specified file info.
    /// </summary>
    /// <param name="folders"></param>
    /// <param name="file"></param>
    /// <returns></returns>
    private FileInfo GetFile(string[] folders, string file) {
        var path = Application.streamingAssetsPath;

        foreach (var item in folders) {
            path = Path.Combine(path, item);
        }

        return new FileInfo(Path.Combine(path, file));
    }
}
