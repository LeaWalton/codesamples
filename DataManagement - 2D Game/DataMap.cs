using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;


/// <summary>
/// DataMap holds the data loaded from file and provides key-data mapping via dictionary.
/// </summary>
public class DataMap<T> where T : IModdable, new() {
    public Dictionary<string, T> data { get; private set; }

    /// <summary>
    /// Default constructor for DataMap object.
    /// </summary>
    public DataMap() {
        data = new Dictionary<string, T>();
    }

    /// <summary>
    /// Get list of keys in DataMap dictionary.
    /// </summary>
    /// <returns>The data keys as a List</returns>
    public List<string> Keys {
        get { return new List<string>(data.Keys); }
    }

    /// <summary>
    /// Get list of values in DataMap dictionary.
    /// </summary>
    /// <returns>The data values as a List</returns>
    public List<T> Values {
        get { return new List<T>(data.Values); }
    }

    /// <summary>
    /// Get data count
    /// </summary>
    /// <returns>The number of stored data</returns>
    public int Count {
        get { return data.Count; }
    }

    /// <summary>
    /// Add the data to the specified Type dictionary.
    /// </summary>
    /// <param name="data"></param>
    public void Add(T data) {
        if (data.Name == null) {
            Debug.LogWarning("data name is null, unable to add item");
            return;
        }

        if (this.data.ContainsKey(data.Name)) {
            Debug.LogWarning(data.Name + " already exists in Data Map. Overwriting.");
            this.data[data.Name] = data;
        }
        else {
            this.data.Add(data.Name, data);
        }
    }
}
