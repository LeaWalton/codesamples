using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System;

/// <summary>
/// Interface for classes that are moddable through file data.
/// </summary>
public interface IModdable {
    /// <summary>
    /// Gets the name of the data type.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Gets the Type keyword of the moddable item.
    /// </summary>
    string Type { get; set; }

    /// <summary>
    /// Read data from file to create new definition of object/need etc.
    /// </summary>
    /// <param name="token"></param>

    void ReadDataJSON(JToken token);

    /// <summary>
    /// Write object definition to serializable format.
    /// </summary>
    /// <returns>The data as an object to serialize</returns>
    JToken WriteDataJSON();
}
