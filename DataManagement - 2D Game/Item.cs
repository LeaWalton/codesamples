using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Item is a buyable/buildable object.
/// </summary>
public class Item : IModdable, IBuildable {
    public string Name { get; set; }
    public string Type { get; set; }

    //Read in from object definition data.
    public bool isFloorObject;
    public bool isNormalObject;
    public bool isWallObject;
    public bool isCeilingObject;
    public bool insideOnly;
    public int sizeX;
    public int sizeY;

    //IBuildable properties
    public Sprite MenuImage { get; private set; }
    public float ConstructTime { get; private set; }
    public string Description { get; private set; }
    public bool Hidden { get; private set; }

    public List<Sprite> sprites;
    
    /// <summary>
    /// Reads from given JToken to populate Item information.
    /// </summary>
    /// <param name="token"></param>
    public void ReadDataJSON(JToken token) {
        Name = (string)token["Name"];

        ConstructTime = token.Value<float?>("Construct Time") ?? 0;
        Description = token.Value<string>("Description") ?? "DESCRIPTION NOT LOADED";
        sprites = GenerateSprites(token["Sprites"]);
        MenuImage = GenerateMenuSprite();
        sizeX = token.Value<int?>("Width") ?? 1;
        sizeY = token.Value<int?>("Height") ?? 1;
        Hidden = token.Value<bool?>("Hidden") ?? false;
        isFloorObject = token.Value<bool?>("Floor Object") ?? false;
        isNormalObject = token.Value<bool?>("Normal Object") ?? true;
        isWallObject = token.Value<bool?>("Wall Object") ?? false;
        isCeilingObject = token.Value<bool?>("Ceiling Object") ?? false;
    }

    /// <summary>
    /// Writes the Item information to a JToken for serialisation.
    /// </summary>
    /// <returns></returns>
    public JToken WriteDataJSON() {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Generates sprite for menu icon.
    /// </summary>
    /// <returns></returns>
    private Sprite GenerateMenuSprite() {
        Rect rect = new Rect(sprites[0].textureRect.x, sprites[0].textureRect.y, 64f, 64f);
        return Sprite.Create(sprites[0].texture, rect, new Vector2(0.5f, 0.5f), 64f);
    }

    /// <summary>
    /// Creates image sprites for the Item from the given texture definition.
    /// </summary>
    /// <param name="spriteDef"></param>
    /// <returns></returns>
    private List<Sprite> GenerateSprites(JToken spriteDef) {
        List<Sprite> sprites = new List<Sprite>();

        foreach (var token in spriteDef) {
            var sd = token.ToObject<SpriteDefinition>();

            var filepath = Path.Combine(Application.streamingAssetsPath, "Textures", sd.spritePath);

            byte[] fileData = File.ReadAllBytes(filepath);
            Texture2D texture = new Texture2D(2, 2);
            texture.LoadImage(fileData);
            texture.filterMode = FilterMode.Point;

            sd.sizeX = sd.sizeX == 0 ? 1 : sd.sizeX;
            sd.sizeY = sd.sizeY == 0 ? 1 : sd.sizeY;

            int unit = 32;

            var bounds = new Rect((sd.startX) * unit, (sd.startY) * unit, sd.sizeX * unit * 2, sd.sizeY * unit * 2);
            var sprite = Sprite.Create(texture, bounds, new Vector2(0.5f, 0.5f), 64f);

            sprites.Add(sprite);
        }


        return sprites;
    }
}
