package com.monash.View;

import com.monash.model.Job;
import com.monash.model.Tag;
import com.monash.model.TagKey;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.NumberFormat;
import java.util.*;

/**
 * A UI form to for creating/editing Job details
 * @author Lea Walton
 * @version 1.0
 */
public class JobForm {
    private static final String CATEGORY = "CATEGORY";
    private static final String SKILL = "SKILL";
    private static final String OPEN = "Open";
    private static final String CLOSED = "Closed";

    private JButton mainButton;
    private JPanel rootPanel;
    private JTextField jobTitle;
    private JTextField company;
    private JTextArea errTitle;
    private JTextField location;
    private JFormattedTextField min;
    private JFormattedTextField max;
    private JTextArea keywords;
    private JComboBox<String> status;
    private JTextArea errCompany;
    private JTextArea errSave;
    private JButton backButton;
    private JTextArea summary;
    private JTextArea description;
    private JTextArea errSummary;
    private JTextArea errDescription;
    private JTextArea errSalary;
    private JPanel catScrollContent;
    private JScrollPane catScroll;
    private ArrayList<JCheckBox> checkBoxes;

    /**
     * Default constructor - generates form in 'create' mode
     */
    public JobForm() {
        status.addItem(OPEN);
        status.addItem(CLOSED);
        NumberFormat nf = NumberFormat.getIntegerInstance();
        NumberFormatter nff = new NumberFormatter(nf);
        DefaultFormatterFactory factory = new DefaultFormatterFactory(nff);
        min.setFormatterFactory(factory);
        max.setFormatterFactory(factory);

        catScrollContent.setLayout(new BoxLayout(catScrollContent, BoxLayout.Y_AXIS));
        catScroll.setBorder(BorderFactory.createEmptyBorder());

        //loadCategories();
        loadCategoriesSorted();

        mainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(validateFields()) {
                    addJob();
                }
            }
        });
        jobTitle.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(jobTitle.getText().isBlank()){
                    errTitle.setEnabled(true);
                    jobTitle.setText(jobTitle.getText().trim());
                }
                else{
                    errTitle.setEnabled(false);
                }

                return true;
            }
        });
        company.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(company.getText().isBlank()){
                    errCompany.setEnabled(true);
                    company.setText(company.getText().trim());
                }
                else{
                    errCompany.setEnabled(false);
                }

                return true;
            }
        });
        summary.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(summary.getText().length() >= 1 && summary.getText().length() <= 250){
                    errSummary.setEnabled(false);
                }
                else{
                    errSummary.setEnabled(true);
                }
                return true;
            }
        });
        description.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(description.getText().length() >= 1 && description.getText().length() <= 1500){
                    errDescription.setEnabled(false);
                }
                else{
                    errDescription.setEnabled(true);
                }
                return true;
            }
        });
        min.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                return verifySalaryNums();
            }
        });
        max.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                return verifySalaryNums();
            }
        });

        errCompany.setEnabled(false);
        errTitle.setEnabled(false);
        errSummary.setEnabled(false);
        errDescription.setEnabled(false);
        errSalary.setEnabled(false);
        errSave.setEnabled(false);

        addEditChangeListeners();
    }

    /**
     * Constructor to generate the jobForm in editing mode.
     * @param editMode  Selection for edit mode as a boolean
     * @param job       The selected job to load as a Job object
     */
    public JobForm(boolean editMode, Job job) {
        this();

        if(editMode && job != null){
            jobTitle.setText(job.getTitle());
            company.setText(job.getCompany());
            location.setText(job.getLocation());
            min.setValue(job.getPayLow());
            max.setValue(job.getPayHigh());
            summary.setText(job.getSummary());
            description.setText(job.getDescription());
            status.setSelectedItem((job.isActive()) ? OPEN : CLOSED);

            displaySelectedCategories(job.getJobCategoriesList());
            displayKeywords(job.getJobSkillsList());

            changeDisplayToEdit(job);
        }

        validateFields();
    }

    /**
     * Adds Listeners to existing form components so that on edit/property changed the 'save success/failed'
     * message is disabled.
     */
    private void addEditChangeListeners() {
        DocumentListener editListener = new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                errSave.setEnabled(false);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                errSave.setEnabled(false);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                errSave.setEnabled(false);
            }
        };

        company.getDocument().addDocumentListener(editListener);
        jobTitle.getDocument().addDocumentListener(editListener);
        min.getDocument().addDocumentListener(editListener);
        max.getDocument().addDocumentListener(editListener);
        location.getDocument().addDocumentListener(editListener);
        keywords.getDocument().addDocumentListener(editListener);
        summary.getDocument().addDocumentListener(editListener);
        description.getDocument().addDocumentListener(editListener);

        status.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                errSave.setEnabled(false);
            }
        });
    }

    /**
     * Initiates adding new job to database through the Controller.
     */
    private void addJob() {
        generateCategoryTags();

        Job newJob = new Job();

        populateJobFields(newJob);

        Date date = new Date();
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        newJob.setDate(sqlDate);

        boolean success = FrameManager.getInstance().getController().addJob(newJob);

        if(success){
            displaySuccess("Created job successfully");

            //Refresh job data for recent add.
            FrameManager.getInstance().getController().loadRecData(FrameManager.getInstance().getController().getSessionUserId());

            try{
                var jobs = FrameManager.getInstance().getController().getRecruiterJobList();

                changeDisplayToEdit(jobs.get(jobs.size()-1));
            }
            catch (Exception e){
                //remain on same form. Disable add button to prevent duplicate job creation.
                //User can still select back
                mainButton.setEnabled(false);
            }
        }
        else{
            displayError();
        }


    }

    /**
     * Change job form display mode to edit mode
     * @param job Job
     */
    private void changeDisplayToEdit(Job job) {
        //Remove existing actions
        for(int i = 0; i < mainButton.getActionListeners().length; i++){
            mainButton.removeActionListener(mainButton.getActionListeners()[i]);
        }

        mainButton.setText("Save Changes");
        mainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(validateFields()){
                    updateJob(job);
                }
            }
        });

    }

    /**
     * Display an error message
     */
    private void displayError() {
        errSave.setForeground(Color.RED);
        errSave.setText("There was an error while saving");
        errSave.setEnabled(true);
    }

    /**
     * Sets the keyword string display for the listed strings
     * @param keywordTags
     */
    private void displayKeywords(ArrayList<String> keywordTags){
        System.out.println("Displaying " + keywordTags.size() + " keywords");

        String keyString = "";

        if(keywordTags == null || keywordTags.isEmpty()){
            return;
        }

        for(String tag : keywordTags){
            if(!tag.isBlank()){
                keyString += tag.toLowerCase() + ", ";
            }
        }

        keyString = keyString.trim();

        keywords.setText(keyString);
    }

    /**
     * Sets the category checkbox selections based for the listed Tags
     * @param categoryTags
     */
    private void displaySelectedCategories(ArrayList<Tag> categoryTags) {
        for (Tag tag : categoryTags) {
            System.out.println("Attempting category match for [tagID: " + tag.getId() + " tagKeyType: " + tag.getKey().getType() + " tagKeyName: " + tag.getKey().getName() + " tagKeyValue: " + tag.getKey().getValue());

            for (JCheckBox box : checkBoxes) {
                if(box.getText().equalsIgnoreCase(tag.getKey().getName())){
                    box.setSelected(true);
                }
                else{
                    System.out.println(tag.getKey().getName() + " doesn't match " + box.getText());
                }
            }
        }

        catScrollContent.revalidate();
        catScrollContent.repaint();
    }

    /**
     * Displays a positive formatted message
     * @param message   The message to display as a String
     */
    private void displaySuccess(String message) {
        errSave.setForeground(new Color(52,107,187));
        errSave.setText(message);
        errSave.setEnabled(true);
    }

    /**
     * Get the selected categories that are to be applied to the Job as a Tag
     * @return  array of categories as Tags.
     */
    private ArrayList<Tag> generateCategoryTags(){
        ArrayList<Tag> catTags = new ArrayList<>();

        for( JCheckBox box : checkBoxes) {
            if(box.isSelected()){
                Tag tag = new Tag();
                TagKey key = new TagKey();
                key.setType(CATEGORY);
                key.setName(box.getText());

                tag.setKey(key);

                catTags.add(tag);
            }
        }
        return catTags;
    }

    /**
     * Method to generate keyword tags from field
     * @return  The list of keyword tags as an ArrayList
     */
    private ArrayList<Tag> generateKeywordTags(){
        ArrayList<Tag> keywordTags = new ArrayList<>();

        String[] keyString = keywords.getText().split(",");

        for(int i = 0; i < keyString.length; i++){
            keyString[i].replaceAll(",","");

            if(!keyString[i].isBlank()){
                Tag tag = new Tag();
                TagKey key = new TagKey();
                key.setType(SKILL);
                key.setName(keyString[i].trim());
                tag.setKey(key);

                keywordTags.add(tag);
            }
        }
        System.out.println("Job has " + keywordTags.size() + " keywords in list");

        return keywordTags;
    }

    /**
     * Accessor method to get the back button
     * @return  The back button as a JButton
     */
    public JButton getBackButton() {
        return backButton;
    }

    /**
     * Accessor method to get the category scroll view
     * @return  The scroller as a JScrollPane
     */
    public JScrollPane getCatScroll() {
        return catScroll;
    }

    /**
     * Accessor method to get the category scroll content
     * @return  The content as a JPanel
     */
    public JPanel getCatScrollContent() {
        return catScrollContent;
    }

    /**
     * Accessor method to get the list of checkboxes
     * @return  The checkbox list as an ArrayList
     */
    public ArrayList<JCheckBox> getCheckBoxes() {
        return checkBoxes;
    }

    /**
     * Accessor method to get the company field
     * @return  The field as a JTextField
     */
    public JTextField getCompany() {
        return company;
    }

    /**
     * Accessor method to get the description field
     * @return  The field as a JTextField
     */
    public JTextArea getDescription() {
        return description;
    }

    /**
     * Accessor method to get the company error
     * @return  The error as a JTextArea
     */
    public JTextArea getErrCompany() {
        return errCompany;
    }

    /**
     * Accessor method to get the description error
     * @return  The error as a JTextArea
     */
    public JTextArea getErrDescription() {
        return errDescription;
    }

    /**
     * Accessor method to get the salary error
     * @return  The error as a JTextArea
     */
    public JTextArea getErrSalary() {
        return errSalary;
    }

    /**
     * Accessor method to get the save error
     * @return  The error as a JTextArea
     */
    public JTextArea getErrSave() {
        return errSave;
    }

    /**
     * Accessor method to get the summary error
     * @return  The error as a JTextArea
     */
    public JTextArea getErrSummary() {
        return errSummary;
    }

    /**
     * Accessor method to get the title error
     * @return  The error as a JTextArea
     */
    public JTextArea getErrTitle() {
        return errTitle;
    }

    /**
     * Accessor method to get the job title field
     * @return  The field as a JTextField
     */
    public JTextField getJobTitle() {
        return jobTitle;
    }

    /**
     * Accessor method to get the location field
     * @return  The field as a JTextField
     */
    public JTextField getLocation() {
        return location;
    }

    /**
     * Accessor method to get the main form button
     * @return  The button as a JButton
     */
    public JButton getMainButton() {
        return mainButton;
    }

    /**
     * Accessor method to get max salary field
     * @return  The field as a JFormattedTextField
     */
    public JFormattedTextField getMax() {
        return max;
    }

    /**
     * Accessor method to get the min salary field
     * @return  The field as a JFormattedTextField
     */
    public JFormattedTextField getMin() {
        return min;
    }

    /**
     * Gets the root JPanel used to display the form.
     * @return
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

    /**
     * Accessor method to get the job status field
     * @return  The field as a JComboBox
     */
    public JComboBox<String> getStatus() {

        return status;
    }

    /**
     * Accessor method to get the summary field
     * @return  The field as a JTextArea
     */
    public JTextArea getSummary() {
        return summary;
    }

    /**
     * Loads categories from the admins configuration into selectable list for recruiter to assign categories.
     */
    private void loadCategories() {
        checkBoxes = new ArrayList<>();
        catScrollContent.removeAll();

        HashMap<Integer,String> categories = FrameManager.getInstance().getController().getCategories();

        for (Map.Entry<Integer,String> entry : categories.entrySet()) {
            JCheckBox check = new JCheckBox(entry.getValue());
            check.setActionCommand(entry.getValue());
            check.setFont(new Font("Arial", Font.PLAIN, 16));
            check.setBackground(Color.white);
            check.setForeground(Color.black);
            check.setMinimumSize(new Dimension(500,30));

            //Edit listener
            check.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    errSave.setEnabled(false);
                }
            });

            checkBoxes.add(check);
            catScrollContent.add(check);
            catScrollContent.add(Box.createRigidArea(new Dimension(-1, 10)));
        }

        catScroll.getVerticalScrollBar().setUnitIncrement(20);
        catScrollContent.revalidate();
        catScrollContent.repaint();
    }

    /**
     * Loads categories from the admins configuration into selectable list for recruiter to assign categories.
     */
    private void loadCategoriesSorted() {
        checkBoxes = new ArrayList<>();
        catScrollContent.removeAll();

        TreeMap<String,Integer> categories = FrameManager.getInstance().getController().getCategorieSorted();

        for (Map.Entry<String,Integer> entry : categories.entrySet()) {
            JCheckBox check = new JCheckBox(entry.getKey());
            check.setActionCommand(entry.getKey());
            check.setFont(new Font("Arial", Font.PLAIN, 16));
            check.setBackground(Color.white);
            check.setForeground(Color.black);
            check.setMinimumSize(new Dimension(500,30));

            //Edit listener
            check.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    errSave.setEnabled(false);
                }
            });

            checkBoxes.add(check);
            catScrollContent.add(check);
            catScrollContent.add(Box.createRigidArea(new Dimension(-1, 10)));
        }

        catScroll.getVerticalScrollBar().setUnitIncrement(20);
        catScrollContent.revalidate();
        catScrollContent.repaint();
    }

    /**
     * Parses the text from the formatted number field to an Integer.
     * @param min Salary as formatted text
     * @return Salary as integer
     */
    private int parseSalary(JFormattedTextField min) {
        return Integer.parseInt(min.getText().replaceAll(",", ""));
    }

    /**
     * Updates the job with data from the form fields
     * @param job The job to update data in
     */
    private void populateJobFields(Job job) {
        int minSalary = parseSalary(min);
        int maxSalary = parseSalary(max);

        job.setTitle(jobTitle.getText());
        job.setCompany(company.getText());
        job.setPayLow(minSalary);
        job.setPayHigh(maxSalary);
        job.setLocation(location.getText());
        job.setActive(status.getSelectedItem().equals(OPEN));
        job.setRecruiterId(FrameManager.getInstance().getController().getSessionUserId());
        job.setDescription(description.getText());
        job.setSummary(summary.getText());

        ArrayList<Tag> allTags = new ArrayList<>();
        allTags.addAll(generateKeywordTags());
        allTags.addAll(generateCategoryTags());

        job.setJobTagList(allTags);
    }

    /**
     * Mutator method to set the back button
     * @param backButton The back button as a JButton
     */
    public void setBackButton(JButton backButton) {
        this.backButton = backButton;
    }

    /**
     * Sets which screen the back button should return to when pressed
     * @param returnToOpen
     */
    public void setBackButtonToOpenJobs(boolean returnToOpen) {
        if(returnToOpen) {
            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    var openJobs = new RecruiterJobSummary(true);
                    FrameManager.getInstance().swapIntoPanel(
                            (JPanel) rootPanel.getParent(), openJobs.getRootPanel()
                    );
                }
            });
        }
        else {
            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    var closedJobs = new RecruiterJobSummary(false);
                    FrameManager.getInstance().swapIntoPanel(
                            (JPanel) rootPanel.getParent(), closedJobs.getRootPanel()
                    );
                }
            });
        }
    }

    /**
     * Mutator method to set the category scroll pane
     * @param catScroll The category scroller as a JScrollPane
     */
    public void setCatScroll(JScrollPane catScroll) {
        this.catScroll = catScroll;
    }

    /**
     * Mutator method to set the category scroll content panel
     * @param catScrollContent The category content panel as a JPanel
     */
    public void setCatScrollContent(JPanel catScrollContent) {
        this.catScrollContent = catScrollContent;
    }

    /**
     * Mutator method to set the category checkboxes
     * @param checkBoxes The checkboxes as an ArrayList of JCheckBox
     */
    public void setCheckBoxes(ArrayList<JCheckBox> checkBoxes) {
        this.checkBoxes = checkBoxes;
    }

    /**
     * Mutator method to set the company text field
     * @param company The company text field as a JTextField
     */
    public void setCompany(JTextField company) {
        this.company = company;
    }

    /**
     * Mutator method to set the job description field
     * @param description The job description field as a JTextArea
     */
    public void setDescription(JTextArea description) {
        this.description = description;
    }

    /**
     * Mutator method to set the error for company field
     * @param errCompany The company field error as a JTextArea
     */
    public void setErrCompany(JTextArea errCompany) {
        this.errCompany = errCompany;
    }

    /**
     * Mutator method to set the error for description field
     * @param errDescription The description error as a JTextArea
     */
    public void setErrDescription(JTextArea errDescription) {
        this.errDescription = errDescription;
    }

    /**
     * Mutator method to set the salary error field
     * @param errSalary The error field as a JTextArea
     */
    public void setErrSalary(JTextArea errSalary) {
        this.errSalary = errSalary;
    }

    /**
     * Mutator method to set the save error field
     * @param errSave The error field as a JTextArea
     */
    public void setErrSave(JTextArea errSave) {
        this.errSave = errSave;
    }

    /**
     * Mutator method to set the summary error field
     * @param errSummary The error field as a JTextArea
     */
    public void setErrSummary(JTextArea errSummary) {
        this.errSummary = errSummary;
    }

    /**
     * Mutator method to set the job title error field
     * @param errTitle The error field as a JTextArea
     */
    public void setErrTitle(JTextArea errTitle) {
        this.errTitle = errTitle;
    }

    /**
     * Mutator method to set the job title field
     * @param jobTitle The job title field as a JTextField
     */
    public void setJobTitle(JTextField jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Mutator method to set the keywords field
     * @param keywords The keywords field as a JTextField
     */
    public void setKeywords(JTextArea keywords) {
        this.keywords = keywords;
    }

    /**
     * Mutator method to set the location field
     * @param location The location field as a JTextField
     */
    public void setLocation(JTextField location) {
        this.location = location;
    }

    /**
     * Mutator method to set the main form button
     * @param mainButton The main button as a JButton
     */
    public void setMainButton(JButton mainButton) {
        this.mainButton = mainButton;
    }

    /**
     * Mutator method to set the max salary field
     * @param max The max salary field as a JFormattedTextField
     */
    public void setMax(JFormattedTextField max) {
        this.max = max;
    }

    /**
     * Mutator method to set the min salary field
     * @param min The min salary field as a JFormattedTextField
     */
    public void setMin(JFormattedTextField min) {
        this.min = min;
    }

    /**
     * Mutator method to set the root display panel
     * @param rootPanel The root display
     */
    public void setRootPanel(JPanel rootPanel) {
        this.rootPanel = rootPanel;
    }

    /**
     * Mutator method to set the status field
     * @param status The status field as a JComboBox
     */
    public void setStatus(JComboBox<String> status) {
        this.status = status;
    }

    /**
     * Mutator method to set the summary field
     * @param summary The field as a JTextArea
     */
    public void setSummary(JTextArea summary) {
        this.summary = summary;
    }

    /**
     * Displays the state of the class
     * @return  The class state as a String
     */
    @Override
    public String toString() {
        return "JobForm{" +
                "mainButton=" + mainButton +
                ", rootPanel=" + rootPanel +
                ", jobTitle=" + jobTitle +
                ", company=" + company +
                ", errTitle=" + errTitle +
                ", location=" + location +
                ", min=" + min +
                ", max=" + max +
                ", keywords=" + keywords +
                ", status=" + status +
                ", errCompany=" + errCompany +
                ", errSave=" + errSave +
                ", backButton=" + backButton +
                ", summary=" + summary +
                ", description=" + description +
                ", errSummary=" + errSummary +
                ", errDescription=" + errDescription +
                ", errSalary=" + errSalary +
                ", catScrollContent=" + catScrollContent +
                ", catScroll=" + catScroll +
                ", checkBoxes=" + checkBoxes +
                '}';
    }

    /**
     * Update the job data with the latest information in form fields.
     * @param job The job to update as a Job object
     */
    private void updateJob(Job job) {
        if (validateFields()) {

            populateJobFields(job);

            boolean success = FrameManager.getInstance().getController().updateJob(job);

            if(success){
                displaySuccess("Saved successfully");
            }
            else{
                displayError();
            }
        }
    }

    /**
     * Triggers verification of fields and checks that no errors are found and displayed.
     * @return True if checks passed, false otherwise
     */
    private boolean validateFields() {
        jobTitle.getInputVerifier().verify(jobTitle);
        company.getInputVerifier().verify(company);
        min.getInputVerifier().verify(min);
        max.getInputVerifier().verify(max);
        summary.getInputVerifier().verify(summary);
        description.getInputVerifier().verify(description);

        if(errSalary.isEnabled() || errSummary.isEnabled() || errDescription.isEnabled() || errTitle.isEnabled() || errCompany.isEnabled()){
            return false;
        }
        return true;
    }

    /**
     * Checks the minimum and maximum salary inputs and determines if there is an error.
     * @return True if checks passed, false otherwise
     */
    private boolean verifySalaryNums() {
        if(min.getText().isBlank() || max.getText().isBlank()){
            errSalary.setEnabled(true);
        }
        try{
            int minSalary = parseSalary(min);
            int maxSalary = parseSalary(max);

            if(minSalary < 0){
                min.setText("0");
            }
            if(minSalary > maxSalary){
                errSalary.setEnabled(true);
            }
            else{
                errSalary.setEnabled(false);
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return true;
    }
}
