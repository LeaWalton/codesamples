# CodeSamples

Samples of my code from various projects I've completed.

## Job Seeker System - Java Swing UI

This sample code is extracted from a project where I helped develop a job management system. The system had recruiter functionalities, and this particular code provided the user with the ability to create or edit a job, for advertisement to job seekers.

## DataManagement - 2D Game

This is a WIP project - the code shown is a small sample of a data management/loading system I have been creating. The data for in-game items can be defined in a JSON file and loaded in, to allow for moddable/customisable items (such as walls, floors, objects) for building in a 2D tycoon style game.

## Hungry Kitties
[Hungry Kitties on Google Play](https://play.google.com/store/apps/details?id=com.HungryDonut.HungryKitties)

I created this game in Unity when I first started learning C#. While the game design and mechanics aren't the most original or unique, it was a fun way to learn the language as well as test out Unity Ad implementation.



